module.exports = {
  /*
   ** Headers of the page
   */
  head: {
    title: "ComparaYa",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: "Template to pages from mejorseguro",
      },
    ],
    link: [{ rel: "icon", type: "image/x-png", href: "./favicon.png" }],
  },
  /*
   ** Customize the progress bar color
   */
  loading: { color: "#3bd600" },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** Run ESLint on save
     */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          //loader: 'eslint-loader',
          exclude: /(node_modules)/,
        });
      }
    },
  },
  // include bootstrap css
  css: [
    "static/css/bootstrap.min.css",
    "static/css/styles.css",
    "static/css/circle.css",
  ],
  // include bootstrap js on startup
  //plugins: [{ src: '~/plugins/filters.js', ssr: false }],
  plugins: [
    { src: "~/plugins/filters.js", ssr: false },
    { src: "~/plugins/apm-rum.js", ssr: false },
  ],

  modules: ["@nuxtjs/axios"],
  // { id: 'GTM-PS23T2X' }],

  env: {
    /*PRODUCCION*/
    urlNewCoreCompara: "https://dev.core-comparaya.com",
    catalogosDirectos: "https://dev.ws-qualitas.com",
    tokenData: "mHf/0x8xqWmYlrjaRWECOzmkksnuNDZv1fBvMLjpI2g=",
    Environment:'DEVELOP',
    urlValidaciones: "https://core-blacklist-service.com/rest",
    urlMonitoreo: "https://core-monitoreo-service.com/v1", //PRODUCCIÓN
    promoCore: "https://dev.core-persistance-service.com", //CORE DESCUENTOS
    sitio: "https://p.comparaya.mx/seguros-para-motos/",
    hubspot:"https://core-hubspot-dev.mark-43.net/deals/landing",
    motorCobro: "https://p.qualitas-comprasegura.com/cliente",
  },
  render: {
    http2: { push: true },
    resourceHints: false,
    gzip: { threshold: 9 },
  },
  router: {
    base: "/seguros-para-motos/",
  },
};
