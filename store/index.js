import Vuex from 'vuex'

import getTokenService from "../plugins/getToken";
import validacionesService from "../plugins/validaciones";
import dbService from "../plugins/configBase";
import cotizacionPromo from "~/plugins/descuentoService.js";
import telApi from "~/plugins/telefonoService.js";
const createStore = () => {
    const validacion = new validacionesService();
    return new Vuex.Store({
        state: {
            ambientePruebas: false,
            cargandocotizacion: false,
            msjEje: false,
            sin_token: false,
            tiempo_minimo: 1.30,
            clickInteraccion: false,
            clickEcommerce: false,
            mostrarBoton: false,
            promo: 'Recibe 20% de desc + 10% adicional + 12 MSI.',
            config: {
                time:"",
                statusCot:false,
                btn:'',
                urlFinal:'thanks',
                aseguradora: '',
                cotizacion: false,
                emision: false,
                descuento: 0,
                telefonoAS: '',
                grupoCallback: '',
                from: '',
                idPagina: 187,
                idMedioDifusion: 2402,
                idCampana: 0,
                idSubRamo: 50,
                habilitarBtnEmision: false,
                habilitarBtnInteraccion: false,
                loading: false,
                urlRedireccion: '',
                idCotizacionNewCore: 0,
                accessToken: '',
                btnEmisionDisabled: false,
                desc: '',
                msi: '',
                promoImg: '',
                promoLabel: '',
                promoSpecial: 'false',
            },
            ejecutivo: {
                nombre: '',
                correo: '',
                id: 0
            },
            formData: {
                urlOrigen:'',
                aseguradora: '',
                marca: '',
                modelo: '',
                descripcion: '',
                detalle: '',
                clave: '',
                cp: '',
                nombre: '',
                telefono: '',
                gclid_field: '',
                correo: '',
                edad: '',
                fechaNacimiento: '',
                genero: '',
                emailValid: '',
                telefonoValid: '',
                codigoPostalValid: '',
                idHubspot: '',
            },
            cotizacion: {
                "Cliente": {
                    "FechaNacimiento": null,
                    "Genero": null,
                    "Direccion": {
                        "CodPostal": null,
                        "Calle": null,
                        "NoExt": null,
                        "NoInt": null,
                        "Colonia": null,
                        "Poblacion": null,
                        "Ciudad": null,
                        "Pais": null
                    },
                    "Edad": null,
                    "TipoPersona": null,
                    "Nombre": null,
                    "ApellidoPat": null,
                    "ApellidoMat": null,
                    "RFC": null,
                    "Ocupacion": null,
                    "CURP": null,
                    "Telefono": null,
                    "Email": null
                },
                "Cotizacion": {
                    "PrimaTotal": 0,
                    "PrimaNeta": 0,
                    "Derechos": null,
                    "Impuesto": 0,
                    "Recargos": null,
                    "PrimerPago": 0,
                    "PagosSubsecuentes": null,
                    "IDCotizacion": null,
                    "CotID": null,
                    "VerID": null,
                    "CotIncID": null,
                    "VerIncID": null,
                    "Resultado": null
                },
                "Emision": {
                    "PrimaTotal": null,
                    "PrimaNeta": null,
                    "Derechos": null,
                    "Impuesto": null,
                    "Recargos": null,
                    "PrimerPago": null,
                    "PagosSubsecuentes": null,
                    "IDCotizacion": null,
                    "Terminal": null,
                    "Documento": null,
                    "Poliza": null,
                    "Resultado": null
                },
                "Descuento": "0",
                "Vehiculo": {
                    "Descripcion": null,
                    "Uso": null,
                    "Marca": null,
                    "Clave": null,
                    "Servicio": null,
                    "Modelo": null,
                    "NoMotor": "",
                    "NoSerie": "",
                    "NoPlacas": "",
                    "CodMarca": "",
                    "CodDescripcion": "",
                    "CodUso": ""
                },
                "Pago": {
                    "Carrier": 0,
                    "MedioPago": null,
                    "NombreTarjeta": null,
                    "Banco": null,
                    "NoTarjeta": null,
                    "MesExp": null,
                    "AnioExp": null,
                    "CodigoSeguridad": null,
                    "NoClabe": null
                },
                "Paquete": "-",
                "Aseguradora": "-",
                "PeriodicidadDePago": 0,
                "Coberturas": [
                    {
                        "DanosMateriales": "-",
                        "DanosMaterialesPP": "-",
                        "RoboTotal": "-",
                        "RCBienes": "-",
                        "RCPersonas": "-",
                        "RC": "-",
                        "RCFamiliar": "-",
                        "RCExtension": "-",
                        "RCExtranjero": "-",
                        "RCPExtra": "-",
                        "AsitenciaCompleta": "-",
                        "DefensaJuridica": "-",
                        "GastosMedicosOcupantes": "-",
                        "MuerteAccidental": "-",
                        "GastosMedicosEvento": "-",
                        "Cristales": "-",
                    }
                ],
                "CodigoError": null,
                "urlRedireccion": null
            }
        },
        actions: {
            getToken() {
                return new Promise((resolve, reject) => {
                    getTokenService.search(dbService.tokenData)
                        .then(resp => {
                            this.state.config.accessToken = resp.data.accessToken
                            localStorage.setItem("authToken", this.state.config.accessToken);
                            resolve(resp)
                        }, error => {
                            reject(error)
                        })
                })
            },
        },
        mutations: {
            cotizacionPromo: function (state) {
                //AGREGAR ELSE IF PARA LOS DIFERENTES CASOS (PARA QUE DE EL DESCUENTO CORRECTO)
                  let aseguradora;
                  if(state.config.aseguradora=="AZTECA"){
                    aseguradora="CHUBB"
                  }else{
                    aseguradora=state.config.aseguradora;
                  }
                  cotizacionPromo
                    .search(aseguradora)
                    .then(resp => {
                      resp = resp.data;
                      if(resp.discount != "Sin Registro"){
                        state.config.descuento = resp.discount
                        }else{
                            state.config.descuento = 0
                        }

                        if (parseInt(resp.delay)) {
                            state.config.time = resp.delay
                            } else {
                            state.config.time = 5000
                            }
                      }).catch(error=>{
                        state.config.descuento = 0;
                        state.config.time = 5000

                    });

                  },
                  telApi: function (state) {
                    try {
                      telApi
                      .search(state.config.idMedioDifusion)
                      .then(resp => { 
                        resp = resp.data;
                        let telefono = resp.telefono;
                        if (parseInt(telefono)) {
                          var tel = telefono + "";
                          var tel2 = tel.substring(2, tel.length);
                          state.config.telefonoAS = tel2;
                        }})
                      .catch(error => {               
                        console.log(error);
                      });
        
                        } catch (error) {
                            console.log(error);
                        }
                    },
              
            validarTokenCore: function (state) {
                try {
                    if (process.browser) {
                        if (localStorage.getItem("authToken") === null) {
                            state.sin_token = true;
                            console.log('NO HAY TOKEN...')
                        } else {
                            state.config.accessToken = localStorage.getItem("authToken");
                            var tokenSplit = state.config.accessToken.split('.');
                            var decodeBytes = atob(tokenSplit[1]);
                            var parsead = JSON.parse(decodeBytes);
                            var fechaUnix = parsead.exp
                            /*
                            * Fecha formateada de unix a fecha normal
                            * */
                            var expiracion = new Date(fechaUnix * 1000);
                            var hoy = new Date(); //fecha actual
                            /*
                            * Se obtiene el tiempo transcurrido en milisegundos
                            * */
                            var tiempoTranscurrido = expiracion - hoy;
                            /*
                            * Se obtienen las horas de diferencia a partir de la conversión de los
                            * milisegundos a horas.
                            * */
                            var horasDiferencia = Math.round(((tiempoTranscurrido / 3600000) + Number.EPSILON) * 100) / 100;

                            if (hoy > expiracion || horasDiferencia < state.tiempo_minimo) {
                                state.sin_token = 'expirado'
                            }
                        }
                    }
                } catch (error2) {
                    console.log(error2)
                }
            },

        }
    })
}
export default createStore
