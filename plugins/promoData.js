import axios from 'axios';

 const apiURL = 'https://api-promos.ahorraseguros.mx/insurers';
// Para pruebas
//const apiURL = 'https://api-promos.cf/insurers';

async function getInsurerData (insurer='', promotype='motos') {
	try {
		const {data} = await axios.get(`${apiURL}/${insurer}`);

		if (!insurer) return data.insurers;

		const gotData = data.insurer;
		const promo = gotData.promotions.find(promo => promo.type === promotype.toLowerCase());

		return {
			name: gotData.name,
			promo
		}
	}
	catch(err) {
		console.log(err)
	}
}

export default getInsurerData;