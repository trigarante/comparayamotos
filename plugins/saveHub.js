import axios from "axios";

const saveDataCliente = {};

saveDataCliente.search = function (data, accessToken) {
  return axios({
    method: "post",
    headers: { 'Content-Type': 'application/json' },
    url: process.env.hubspot,
    data: JSON.parse(data),
  });
};
export default saveDataCliente;
