import axios from "axios";

const cotizacionService = {};

cotizacionService.search = function (peticion, accessToken) {
  return axios({
    method: "post",
    headers: { Authorization: `Bearer ${accessToken}` },
    // url: "https://core-persistance-service.com/v2/qualita/motorcycle/quotation",
    url: process.env.promoCore + "/v2/qualitas/motorcycle/quotation",
    data: JSON.parse(peticion),
  });
};
export default cotizacionService;
