import axios from 'axios'
import configDB from './configBase'

const cotizacionService = {}

cotizacionService.search = function (peticion, accessToken) {

    return axios({
        method: "post",
        headers: { Authorization: `Bearer ${accessToken}` },
        url: configDB.baseUrl+  '/v1/motos',
        data: JSON.parse(peticion)
    })
}
export default cotizacionService


