import axios from "axios";

const newLeadEmision = {};

newLeadEmision.newLeadEm = function (peticion, accessToken) {
  return axios({
    method: "post",
    headers: { Authorization: `Bearer ${accessToken}` },
    url: process.env.promoCore + "/v3/interaction/request_online",
    data: JSON.parse(peticion),
  })
    .then((res) => res.data)
    .catch((err) =>
      console.error("Ups... no se pudo guardar la nueva interaccion :( " + err)
    );
};
export default newLeadEmision;
