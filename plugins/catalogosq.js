import axios from 'axios'

// let catalogo = process.env.catalogo + "/catalogos";
// let urlConsumo = process.env.urlNewCoreCompara + '/v1'
let urlConsumo = process.env.catalogosDirectos + '/v3/qualitas-motos'


class Catalogos {
  marcas() {
    return axios({
      method: "get",
      url: urlConsumo + '/brands',
    })
  }
  modelos(marca) {
    return axios({
      method: "get",
      url: urlConsumo + `/years?brand=${marca}`,
      headers: {'Access-Control-Allow-Origin': '*',},
    })
  }
  submarcas(marca, modelo) {
    return axios({
      method: "get",
      url: urlConsumo + `/models?brand=${marca}&year=${modelo}`,
      headers: {'Access-Control-Allow-Origin': '*',},
    })
  }
  descripciones(marca, modelo, submarca) {
    return axios({
      method: "get",
      url: urlConsumo + `/variants?brand=${marca}&year=${modelo}&model=${submarca}`,
      headers: {'Access-Control-Allow-Origin': '*',},
    })
  }
}

export default Catalogos;
