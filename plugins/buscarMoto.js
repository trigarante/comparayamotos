import axios from 'axios'
import configDB from './configBase'

const buscarMotoService ={};

buscarMotoService.search=function (aseguradora,marca, modelo, descripcion,subdescripcion,detalle, accessToken) {
  return axios.get(configDB.baseUrl+`/buscar_motos?aseguradora=${aseguradora}&marca=${marca}&modelo=${modelo}&descripcion=${descripcion}&subdescripcion=${subdescripcion}&detalle=${detalle}`,{
    headers: {Authorization: `Bearer ${accessToken}`},
  })
}
export default buscarMotoService
