import axios from "axios";

const telApi = {};
telApi.search = function (idDiffusionMedium) {
  return axios({
    method: "get",
    url:
      process.env.promoCore +
      "/v1/page/diffusion-medium/phone?idDiffusionMedium=" +
      idDiffusionMedium,
  });
};
export default telApi;
