import { init as initApm } from '@elastic/apm-rum'
const apm = initApm({
 // Set required service name (allowed characters: a-z, A-Z, 0-9, -, _, and space)
//SE COLOCA LA DIRECCIÓN DEL SITIO SIN PUNTOS
 serviceName: 'comparayaMx-seguros-para-moto',
 // Set custom APM Server URL (default: http://localhost:8200)
 // serverUrl: 'http://34.235.19.168:8200',
 serverUrl: 'https://trackingfront.com',
 distributedTracingOrigins: ['https://trackingfront.com'],
 // Set service version (required for sourcemap feature)
  serviceVersion: '2.0',
   // environment:'PRODUCTION'
   environment:process.env.Environment
})
