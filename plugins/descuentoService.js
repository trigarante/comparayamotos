import axios from "axios";
const cotizacionPromo = {};
cotizacionPromo.search = function (aseguradoraP) {
  return axios({
    method: "get",
    url:
      process.env.promoCore +
      "/v1/discount/" +
      aseguradoraP +
      "?business=AS&service=MOTOS",
  });
};
export default cotizacionPromo;
