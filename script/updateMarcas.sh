#! /bin/bash
pwd
#Se comprueba que exista el directorio backup dentro de la carpeta script
#En caso de que no exista, se va a crear
if [ -d script/backup ];
then
echo "Sí, sí existe backup."
else
pwd
echo "No, no existe backup, se creará."
cd script || exit
mkdir backup
ls -la
cd ..
fi
#Se indica la aseguradora a la cual se desea consultar las marcass
aseguradora='qualitas'
if [ -f script/$aseguradora"_motos".json ];
  then
    echo "Sí, sí existe, movere archivos"
    mv script/$aseguradora"_motos".json script/backup/
  else
    echo "No existe no movere archivos"
  fi

pet="$(curl https://dev.ws-qualitas.com/v1/qualitas-motos/brands)"
aseguradora=$aseguradora"_motos"
  if [[ $pet =~ ^\[ ]]; then
   echo "Es un array de marcas"
   if [[ ! $pet =~ ^\[\] ]]; then
     echo "Hay marcas en el array, se guardarán en el json"
     echo $pet >> script/$aseguradora.json
   else
     echo "El Array está vacio, se traerán las marcas del backup"
     cp script/backup/$aseguradora.json script/$aseguradora.json
   fi
  else
   echo "No, no es un array, se traerán las marcas del backup"
   cp script/backup/$aseguradora.json script/$aseguradora.json
  fi
