#! /bin/bash
grep -rl 'https://dev.' nuxt.config.js | xargs sed -i 's/https:\/\/dev./https:\/\//g'
grep -rl 'https://dev.' script/updateMarcas.sh | xargs sed -i 's/https:\/\/dev./https:\/\//g'
grep -rl 'https://p.' nuxt.config.js | xargs sed -i 's/https:\/\/p./https:\/\//g'
grep -rl '_blank' components/common/botones.vue | xargs sed -i 's/_blank/_self/g'
#Hubspot
grep -rl 'core-hubspot-dev.mark-43.net' nuxt.config.js | xargs sed -i 's/core-hubspot-dev.mark-43.net/core-hubspot-prod.mark-43.net/g'